# Terraform-demo

This purpose of this repo is to store the files for the terraform presentation. It contains the different examples in a separate branch.

## What is Terraform ?

Terraform is a tool to provide and manage an infrastructure as code. 

We declare our infrastructure in a bunch of terraform file (.tf)  then we execute some some commands to apply it.

### The workflow

1. Code !
2. `terraoform validate` to ensure that the configuration is valid.
3. `terraform plan` , this command does a simulation of what will append if we apply our configuration. 
4. `terraform apply` 
5. `terraform destroy` to destroy all our infrastructure.

### Useful knowledge

- We can give the  `plan` command a flag `-out=file.tfplan` to export the plan in a file.

- The `apply` command will **always** check the differences between the new configuration and the actual configuration with a local `terraform.tfsate` file. If the file does not exist, that means for Terraform that there is no actual configuration on the cloud provider. Si if we lost this file there is no way to update or delete the infrastructure terraform will create new instances.

  The good news is we can have multiple configurations for one cloud provider account. As we can keep all the `.tfstate` file separated



## Branches

### basic-file

In this case we present a simple terraform configuration with an Exoscale Micro Instance, a security group with 2 security rule one for the port 22 and one for the port 80 and an ssh-key. It's the minimal configuration for an instance.

### basic-file-ansible

This example extends from the basic-file, we added an Ansible playbook configuration to install git on the instance.

Ansible needs the ip address to connect on the host, but the problem is that the address is a "calculated" property and we know it only once the instance is created.

Terraform has a module called "template_file" that allows us to create a file with in there some variables that will be replaced by terraform.

To pass the ip address to the ansible playbook, we use the `hosts.tpl` with an interpolatable variable `${ip}` in it.

```
data "template_file" "ansible-hosts-template" {
  template = "${file("ansible/hosts.tpl")}"
  vars = {
    ip = exoscale_compute.instance1.ip_address
  }
  depends_on = [exoscale_compute.instance1]
}
```

We can tell terraform that our template depends on the creation of the instance, to ensure that we know the ip adress before running ansible.

Then to render th template :

we can use : `data.template_file.ansible-hosts-template.rendered`

Or create a local file :

```
resource "local_file" "ansible-hosts-file" {
  content  = data.template_file.ansible-hosts-template.rendered
  filename = "hosts"
}
```

Note : the filename property can be a relative path from terraform file.

### basic-gitlab-cicd

In this example we added a gitlab-ci configuration that will follow the terraform workflow (validate ->plan -> apply -> destroy)

Note: be sure to have the environment variables set in gitlab's configuration.

### basic-module

To have a terraform code more readable, we can create modules. There are like regular code functions. Basically a module is a folder. Remember, when we execute a terraform command in terminal, it will execute all file in the folder.

When we code a function we give some parameter and expect some return value. So in terraform we have a keyword `variable` and `output`.

In the module we can access the variable : `var.variablename`

#### Call a module

In our root terraform file we can just have this :

```
module "app1" {
  source        = "./modules/exo-template"
  instance_name = "project1-prod"
  ssh_key       = var.ssh_key
}
```

Note: that `var.ssh_key` is a variable of the root module

We need to give the module :

- a name : here "app1"

- a source, where to fetch the module: here in the `./modules/exo-template` folder
- and some variable the same as set in the module folder.




