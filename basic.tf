variable "secret" {}
variable "key" {}
variable "ssh_key" {}

provider "exoscale" {
  version = "~> 0.15"
  secret  = var.secret
  key     = var.key
}


resource "exoscale_ssh_keypair" "admin" {
  name = "terraform_exoscale"
  public_key = var.ssh_key
}

data "exoscale_compute_template" "ubuntu" {
  zone = "ch-gva-2"
  name = "Linux Ubuntu 18.04 LTS 64-bit"
}

resource "exoscale_security_group" "common" {
  name        = "common"
  description = "The common security groupe that contains the higher level of security rules"
}


resource "exoscale_security_group_rule" "ssh" {
  type              = "INGRESS"
  security_group_id = exoscale_security_group.common.id
  protocol          = "TCP"
  start_port        = 22
  end_port          = 22
  cidr              = "0.0.0.0/0"
}

resource "exoscale_security_group_rule" "web" {
  type              = "INGRESS"
  security_group_id = exoscale_security_group.common.id
  protocol          = "TCP"
  start_port        = 80
  end_port          = 80
  cidr              = "0.0.0.0/0"
}


resource "exoscale_compute" "instance1" {
  zone = "ch-gva-2"
  display_name = "instance1"
  size = "Micro"
  disk_size = 10
  keyboard = "fr-ch"
  key_pair = "terraform_exoscale"
  state = "Running"
  security_groups = ["common"]
  depends_on = [exoscale_security_group_rule.web]

  connection {
    type = "ssh"
    host = self.ip_address
    user = data.exoscale_compute_template.ubuntu.username
  }
  template =  data.exoscale_compute_template.ubuntu.name
}